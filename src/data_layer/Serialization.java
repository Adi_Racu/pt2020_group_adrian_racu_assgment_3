package data_layer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import business_Layer.Restaurant;

public class  Serialization {
	
public static void Serializing(Restaurant rest) throws IOException {
	
FileOutputStream fileOutputStream= new FileOutputStream("E:\\Master\\Tehnici_fundamentale\\Tema_3\\restaurant.ser");	
ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);	
objectOutputStream.writeObject(rest);	
}	
	
public static Restaurant DeSerializing() throws IOException, ClassNotFoundException {
	FileInputStream fileInputStream= new FileInputStream("E:\\\\Master\\Tehnici_fundamentale\\Tema_3\\restaurant.ser");	
	ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);	
	Restaurant rest_save=(Restaurant) objectInputStream.readObject();
	objectInputStream.close(); 
	return rest_save;
}		

}

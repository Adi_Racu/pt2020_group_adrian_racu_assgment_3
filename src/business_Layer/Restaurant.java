package business_Layer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Restaurant implements IRestaurantProcessing, Serializable {
List<CompositeProduct> menu=new ArrayList<CompositeProduct>();
List<CompositeProduct> item_orderd=new ArrayList<CompositeProduct>();
Map<Order,List<CompositeProduct>>order_list=new HashMap<Order,List<CompositeProduct>>();

   
    
	public void create_menu_item(String name,long pret) {
	CompositeProduct comp_prod=new CompositeProduct(name,pret);
		menu.add(comp_prod);
		
	}
	
	public void delete_menu_item(String name,long pret) {
	for(CompositeProduct item1 :menu)  {
		 if(item1.get_name().equals(name)) {
		menu.remove(item1);	 
		    } 
	  }
	}
	
	public void edit_menu_item(String name,long pret) {
    CompositeProduct edit_item=new CompositeProduct(name,pret);	
	for(CompositeProduct menu_item: menu) {
		if (menu_item.get_name().equals(name)) {
		int index=menu.indexOf(menu_item);
		menu.set(index, edit_item);
		     }
	  }
	}
	
	public List<CompositeProduct> get_menu() {
		return menu;
		}
	public long compute_price(CompositeProduct prod,int buc) {
		long price;
		price=prod.computePrice(buc);
	 return price;
	}

	public int gen_bill() {
		return 0;
	}
	
	public List<CompositeProduct> add_item(String name,int pret) {
		CompositeProduct item_addet=new CompositeProduct(name,pret);	
		item_orderd.add(item_addet);
		
		/*for(CompositeProduct menu_item: menu) {
			if (menu_item.get_name().equals(name)) {
			item_addet=menu_item;	
			
			     }
		  }*/

		
		return item_orderd;
	}
	
	public Map<Order,List<CompositeProduct>> addto_order(Order new_order,List<CompositeProduct> item_orderd) {
		if(item_orderd.isEmpty()!=true) {
		order_list.put(new_order, item_orderd);}
		return order_list;
	}

	@Override
	public Order create_order(int OrderID, String Date, int Table) {
		Order current_order=new Order(OrderID,Date,Table);
		return current_order;
	}

	
	

	
	

 
	
	

	
	
	

}

package business_Layer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CompositeProduct implements MenuItem, Serializable {
	int numar;
	String name;
	long pret;	
	long price;
private List<MenuItem>  menues =new ArrayList<MenuItem>();	

	public CompositeProduct() {
	}
	public CompositeProduct(String name,long pret) {
	this.name=name;
	this.pret=pret;
	}		

public void add(MenuItem m) {
this.menues.add(m);
}
public void Removemenu(MenuItem m) {
this.menues.remove(m);
}
public long computePrice(int number) {
this.numar=number;
price=numar*pret;
return price;
}

public int get_nr() {
	return numar;
};
public String  get_name() {
	return name;
};
public long  get_pret() {
	return price;
}
public String toString() {
	return"{\"name\":"+name+",\"price\":\""+price+"\" }";
}
}

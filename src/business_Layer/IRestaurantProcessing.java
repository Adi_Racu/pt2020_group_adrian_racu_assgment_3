package business_Layer;

import java.util.List;

public interface IRestaurantProcessing {
public void create_menu_item(String name,long pret);
public void delete_menu_item(String name,long pret);
public void edit_menu_item(String name,long pret);
public Order create_order(int OrderID,String Date,int Table);
public long compute_price(CompositeProduct prod,int buc);
public int gen_bill();
	
}



package business_Layer;

public interface MenuItem  {
public String  get_name();
public long  get_pret();
public int get_nr();
public long computePrice(int number);
public void add(MenuItem m);
public void Removemenu(MenuItem m);
}

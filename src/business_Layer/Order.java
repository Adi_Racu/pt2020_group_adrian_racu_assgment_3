package business_Layer;

import java.io.Serializable;

public class Order implements Serializable {
int OrderID;
String Date;
int Table;
public Order() {}
public Order(int OrderID,String Date,int Table) {
	this.OrderID=OrderID;
	this.Date=Date;
	this.Table=Table;
}
public int orderID() {
	return OrderID;
};
public String Date() {
	return Date;
};
public int Table() {
	return Table;
}

public int get_OrderID() {
	return OrderID;
}
public String get_Date() {
	return Date;
}
public int get_Table() {
	return Table;
}


@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((Date == null) ? 0 : Date.hashCode());
	result = prime * result + OrderID;
	result = prime * result + Table;
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Order other = (Order) obj;
	if (Date == null) {
		if (other.Date != null)
			return false;
	} else if (!Date.equals(other.Date))
		return false;
	if (OrderID != other.OrderID)
		return false;
	if (Table != other.Table)
		return false;
	return true;
};
public String toString() {
	return "{\"Order ID\":"+OrderID+", \"Date\":\""+Date+",\"Table\""+Table+"}";
}

}

package presentation_Layer;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import business_Layer.CompositeProduct;
import business_Layer.MenuItem;
import business_Layer.Order;
import business_Layer.Restaurant;
import data_layer.Serialization;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.table.TableModel;

public class Admin extends JFrame {

public Admin(Restaurant rest) {
	this.rest=rest;
initComponents();	
	
}	
	
	private JPanel contentPane;
	private JTextField name_field;
	private JTextField price_field;
	private JButton Adauga;
	private JButton Delete;
	private JButton Modifica;
	JButton save;
	Restaurant rest=new Restaurant();
	List<CompositeProduct> menu=new ArrayList<CompositeProduct>();
	String columnNames[]= {"Nume","Pret"};
	Object[][] rowdata = {};
	private JTable table;	 
	
	DefaultTableModel model = new DefaultTableModel(rowdata,columnNames);	
	
		 
	 public void refreshInfo() {
		 menu=rest.get_menu();
    	for(CompositeProduct item1 :menu)  {
			 if(item1!=null) 
			 {   model.addRow(new Object[]{item1.get_name(),item1.get_pret()});
			 
			    } 
	 }
		
    
	  }
	  public void clear_table() {
		  model.setRowCount(0); 
	  }	 
		  
	public void initComponents() {
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 596, 300);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		Adauga = new JButton("Adauga");
		Adauga.setBounds(40, 52, 89, 23);
		contentPane.add(Adauga);
		
	    Delete = new JButton("Delete");
		Delete.setBounds(40, 86, 89, 23);
		contentPane.add(Delete);
		
		name_field = new JTextField();
		name_field.setBounds(206, 52, 61, 20);
		contentPane.add(name_field);
		name_field.setColumns(10);
		
		price_field = new JTextField();
		price_field.setBounds(206, 107, 61, 20);
		contentPane.add(price_field);
		price_field.setColumns(10);
		
		JLabel Name = new JLabel("Name");
		Name.setBounds(150, 56, 46, 14);
		contentPane.add(Name);
		
		JLabel lblNewLabel_2 = new JLabel("Pret");
		lblNewLabel_2.setBounds(150, 110, 46, 14);
		contentPane.add(lblNewLabel_2);
		
		Modifica = new JButton("Modifica");
		Modifica.setBounds(40, 120, 89, 23);
		contentPane.add(Modifica);
		 		
		save = new JButton("Save session");
	    save.setBounds(40, 154, 107, 23);
	    contentPane.add(save);
	        
		JScrollPane panel = new JScrollPane();
		panel.setBackground(Color.PINK);
		panel.setBounds(310, 21, 260, 205);
		contentPane.add(panel);
		
        Adauga.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent evt) {
        	String s1 = name_field.getText();
        	String s2 = price_field.getText();
        	rest.create_menu_item(s1, Integer.valueOf(s2).intValue());
        	clear_table();
        	refreshInfo();
		}});
        
        Delete.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent evt) {
        	String s1 = name_field.getText();
        	String s2 = price_field.getText();
        	rest.delete_menu_item(s1, Integer.valueOf(s2).intValue());
        	clear_table();
        	refreshInfo();
		}});
        
        Modifica.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent evt) {
        	String s1 = name_field.getText();
        	String s2 = price_field.getText();
        	rest.edit_menu_item(s1, Integer.valueOf(s2).intValue());
        	clear_table();
        	refreshInfo();
		}});
        
        save.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent evt) {
        	try {
				Serialization.Serializing(rest);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}});
         table=new JTable(model);
        panel.add(table);
        panel.setViewportView(table);
        
       }
	public void showIt() {
		this.setVisible(true);
	}
     }

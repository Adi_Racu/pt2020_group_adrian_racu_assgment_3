package presentation_Layer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import business_Layer.CompositeProduct;
import business_Layer.MenuItem;
import business_Layer.Order;
import business_Layer.Restaurant;
import data_layer.WriteText;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

public class Waiter extends JFrame {
 public Waiter(Restaurant rest) {
	this.rest=rest;
  initComponents();	  
	  
  }
	
 
	//GUI variables
	private JPanel contentPane;
	private JTextField OrderID_field;
	private JTextField date_field;
	private JTextField table_field;
	private JTextField name_field;
    private JTextField buc_field;
    private JButton Order;
	private JButton adauga;
	private JButton bill;
	private JButton compute;
	Restaurant rest=new Restaurant();
	String columnNames[]= {"OrderID","Date","Table"};
	String columnNames2[]= {"OrderID","name","pret"};
	Object[][] rowdata = {};
	Object[][] rowdata2 = {};
	private JTable table;
	private JTable order_table;
	DefaultTableModel model = new DefaultTableModel(rowdata,columnNames);
	DefaultTableModel model_order = new DefaultTableModel(rowdata2,columnNames2);
	//appilcation variables
	long price;
	Order current_order=new Order();	
	Map<Order,List<CompositeProduct>>order_list=new HashMap<Order,List<CompositeProduct>>();	 
	List<CompositeProduct> item_orderd=new ArrayList<CompositeProduct>();	 
	List<CompositeProduct> bill_item=new ArrayList<CompositeProduct>();	 
	List<Order> table_bill=new ArrayList<Order>();
	private JPanel panel_1;
	private JTextField textField;
	private JLabel total_camp;
	private JLabel Total;
	private JLabel lblNewLabel_1;
	public void refreshInfo() {
		for(Order row_item: table_bill) {
			model.addRow(new Object[]{row_item.get_OrderID(),row_item.get_Date(),row_item.get_Table()});
		}
	 }
	
	public void refreshOrder() {
		Order current_order=new Order();
		List<CompositeProduct> bill_element=new ArrayList<CompositeProduct>();	
	for(Map.Entry<Order,List<CompositeProduct>> me :order_list.entrySet())
         {current_order=me.getKey();
         bill_element=me.getValue();
           for(CompositeProduct item: bill_element) {  
		model_order.addRow(new Object[]{current_order.get_OrderID(),item.get_name(),item.get_pret()});
            }
           }
	 }
	
	 public void clear_table() {
		  model.setRowCount(0); 
	  }	 
	 public void clear_table_order() {
		 model_order.setRowCount(0); 
	  }	 	  
	
	public void initComponents() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 732, 511);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
	    Order = new JButton("New order");
		Order.setBounds(51, 39, 89, 23);
		contentPane.add(Order);
		
		OrderID_field = new JTextField();
		OrderID_field.setBounds(261, 40, 86, 20);
		contentPane.add(OrderID_field);
		OrderID_field.setColumns(10);
		
		date_field = new JTextField();
		date_field.setBounds(261, 93, 86, 20);
		contentPane.add(date_field);
		date_field.setColumns(10);
		
		table_field = new JTextField();
		table_field.setBounds(261, 149, 86, 20);
		contentPane.add(table_field);
		table_field.setColumns(10);
		
		JLabel OrderID = new JLabel("OrderID");
		OrderID.setBounds(187, 43, 46, 14);
		contentPane.add(OrderID);
		
		JLabel Date = new JLabel("Date");
		Date.setBounds(187, 96, 46, 14);
		contentPane.add(Date);
		
		JLabel lblNewLabel_2 = new JLabel("Table");
		lblNewLabel_2.setBounds(187, 152, 46, 14);
		contentPane.add(lblNewLabel_2);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		panel.setBounds(24, 180, 306, 262);
		contentPane.add(panel);
		
		adauga = new JButton("Adauga ");
		adauga.setBounds(34, 50, 89, 23);
		panel.add(adauga);
		
		name_field = new JTextField();
		name_field.setColumns(10);
		name_field.setBounds(196, 51, 86, 20);
		panel.add(name_field);
		
		buc_field = new JTextField();
		buc_field.setColumns(10);
		buc_field.setBounds(196, 107, 86, 20);
		panel.add(buc_field);
		
		JLabel label = new JLabel("nume");
		label.setBounds(140, 54, 46, 14);
		panel.add(label);
		
		JLabel lblNumarBucati = new JLabel("numar \r\nbucati");
		lblNumarBucati.setBounds(140, 102, 53, 30);
		panel.add(lblNumarBucati);
		
		bill = new JButton("Bill");
		bill.setBounds(34, 163, 89, 23);
		panel.add(bill);
		
		compute = new JButton("Compute");
		compute.setBounds(34, 107, 89, 23);
		panel.add(compute);
		
		textField = new JTextField();
		textField.setBounds(196, 164, 86, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		JLabel lblPret = new JLabel("pret");
		lblPret.setBounds(140, 159, 53, 30);
		panel.add(lblPret);
		
		JScrollPane scroll = new JScrollPane();
		scroll.setBackground(Color.PINK);
		scroll.setBounds(378, 39, 289, 162);
		contentPane.add(scroll);
		
		Order.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent evt) {
			int order_id=Integer.parseInt(OrderID_field.getText());
			String s1 =date_field.getText();
			int table=Integer.parseInt(table_field.getText());
		 current_order=rest.create_order(order_id,s1,table);
		 table_bill.add(current_order);
		 clear_table();
		 refreshInfo();
			}});
		
		adauga.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent evt) {
			String name=name_field.getText();
			int gramaj=Integer.parseInt(buc_field.getText());
			int pret=Integer.parseInt(textField.getText());
			item_orderd=rest.add_item(name,pret);
			}});	
		
		compute.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent evt) {
			int buc=Integer.parseInt(buc_field.getText());
			order_list=rest.addto_order(current_order,item_orderd);
			bill_item=order_list.get(current_order);
			for(CompositeProduct menu_item: bill_item) {
			price=menu_item.computePrice(buc);	
			total_camp.setText(Long.toString(price));
			}
			
			clear_table_order();
			refreshOrder();
			}});
		
		bill.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent evt) {	
			Order current_order=new Order();
			List<CompositeProduct> bill_element=new ArrayList<CompositeProduct>();	
		for(Map.Entry<Order,List<CompositeProduct>> me :order_list.entrySet())
	         {current_order=me.getKey();
	         bill_element=me.getValue();
	         String order=current_order.toString();
	           for(CompositeProduct item: bill_element) {  
			 String items=item.toString();
			WriteText write=new WriteText();
			
			try {
				write.textCreate(order.concat(items));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	            }
	           }
			
		}});
		table=new JTable(model);
		scroll.add(table);
		scroll.setViewportView(table);
		
	    JScrollPane	panel_1 = new JScrollPane();
	    panel_1.setBackground(Color.PINK);
		panel_1.setBounds(378, 231, 289, 151);
		contentPane.add(panel_1);
		 
		order_table=new JTable(model_order);
		panel_1.add(order_table);
		panel_1.setViewportView(order_table);
		
		total_camp = new JLabel("New label");
		total_camp.setBounds(602, 406, 46, 14);
		contentPane.add(total_camp);
		
		Total = new JLabel("Total");
		Total.setBounds(523, 406, 46, 14);
		contentPane.add(Total);
		
		lblNewLabel_1 = new JLabel("Bill");
		lblNewLabel_1.setBounds(378, 212, 89, 14);
		contentPane.add(lblNewLabel_1);
	}  
	public void showIt() {
	this.setVisible(true);
    }	
	  }
	

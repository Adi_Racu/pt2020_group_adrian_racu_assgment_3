package presentation_Layer;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import business_Layer.Restaurant;
import data_layer.Serialization;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class Start extends JFrame {
static Restaurant rest=new Restaurant();
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {Restaurant rest_save=new Restaurant();
				     Restaurant rest_normal=new Restaurant();
				   Start frame=new Start();
				   Serialization.Serializing(rest);
				   rest_save=Serialization.DeSerializing();
					if(rest_save!=null) {
					frame = new Start(rest_save);
					}
					else {
					frame=new Start(rest_save);
					}
					frame.setVisible(true);	
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
    public Start() {}

	public Start(Restaurant rest) {
		
		this.rest=rest;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 173, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton waiter = new JButton("Waiter");
		waiter.setBounds(33, 95, 89, 23);
		contentPane.add(waiter);
		waiter.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
			 Waiter wait=new Waiter(rest);	
			 wait.showIt();
			}
		});
		
		JButton Administrator = new JButton("Administrator");
		Administrator.setBounds(33, 157, 97, 23);
		contentPane.add(Administrator);
		Administrator.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				 Admin add=new Admin(rest);	
				 add.showIt();
				}
			});
		
		JLabel Rol = new JLabel("Rol");
		Rol.setHorizontalAlignment(SwingConstants.TRAILING);
		Rol.setBounds(10, 32, 70, 14);
		contentPane.add(Rol);
	}
	
	
	
}
